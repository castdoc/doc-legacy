# doc-legacy

This repository is used only to redirect client requests to `doc-legacy.castsoftware.com` to `doc.castsoftware.com`. This is defined in the file `_redirects`. A site called `doc-legacy` is configured in Cloudflare Pages and uses this repository in the `Hugo` build process. 

`doc-legacy.castsoftware.com` is a domain name that was applied temporarily to the on-premises Confluence doc instance before the documentation migration to Gitlab/Cloudflare, therefore we use this redirect mechanism as a "fall back" in case links using doc-legacy.castsoftware.com existing "in the wild" (email, browse favourites etc.).